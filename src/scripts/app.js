import { AttractionsList } from "./components/AttractionsList";

class InputFile {
    constructor() {
        this.selectors();
        this.events();
    }

    selectors() {
        this.imageInputEl = document.querySelector(".form-image-input");
        this.imagePreview = document.querySelector(".form-image-preview");
    }

    events() {
        this.imageInputEl.addEventListener("change", this.handleImage.bind(this), false);
    }

    readURL(file) {
        return new Promise((res, rej) => {
            const reader = new FileReader();
            reader.onload = e => res(e.target.result);
            reader.onerror = e => rej(e);
            reader.readAsDataURL(file);
        });
    };

    async handleImage() {
        const image = this.imageInputEl.files[0];
        const url = await this.readURL(image);

        this.imagePreview.src = url;
        this.imagePreview.style.display = "block";
    }
}

document.addEventListener("DOMContentLoaded", function() {
    new AttractionsList();
    new InputFile();
});
