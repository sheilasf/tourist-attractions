export class AttractionsList {
    constructor() {
        this.list = [
            {
                title: "Pão de Açúcar",
                description:
                    "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
                image: "/images/pao-de-acucar.png",
            },
            {
                title: "Ilha Grande",
                description:
                    "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
                image: "/images/ilha-grande.png",
            },
            {
                title: "Cristo Redentor",
                description:
                    "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
                image: "/images/cristo-redentor.png",
            },
            {
                title: "Centro Histórico de Paraty",
                description:
                    "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
                image: "/images/centro-historico-paraty.png",
            },
        ];

        this.selectors();
        this.events();
        this.renderListItems();
    }

    selectors() {
        this.form = document.querySelector(".attractions-form");
        this.itemImage = document.querySelector(".form-image-input");
        this.previewImage = document.querySelector(".form-image-preview");
        this.itemTitle = document.querySelector(".form-title");
        this.itemDescription = document.querySelector(".form-description");
        this.items = document.querySelector(".attractions-items");
    }

    events() {
        this.form.addEventListener("submit", this.addItemToList.bind(this));
    }

    addItemToList(event) {
        event.preventDefault();

        const itemImage = event.target["image-preview"].src;
        const itemTitle = event.target["title-input"].value;
        const itemDescription = event.target["description-input"].value;

        if (itemImage && itemTitle && itemDescription != "") {
            const item = {
                image: itemImage,
                title: itemTitle,
                description: itemDescription,
            };

            this.list.push(item);

            this.renderListItems();
            this.resetInputs();
        }
    }

    renderListItems() {
        let itemsStructure = "";

        this.list.forEach(function (item) {
            itemsStructure += `
                <div class="attractions-item">
                    <img class="image-result" src="${item.image}"/>
                    <div class="image-result-text">
                        <h3 class="image-result-title">${item.title}</h3>
                        <p class="image-result-description">${item.description}</p>
                    </div>
                </div>
            `;
        });

        this.items.innerHTML = itemsStructure;
    }

    resetInputs() {
        this.itemImage.src = "/";
        this.previewImage.style.display = "none";
        this.itemTitle.value = "";
        this.itemDescription.value = "";
    }
}
